## UDS A1SUBDEMO Report Page
## Modified: 04/26/2016

$page.setTitle("A1SUBDEMO")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)


#parse("macros/cnda_xnatMacros.vm")

#if ($turbineUtils.GetPassedParameter("popup", $data))
   #set ($popup = $turbineUtils.GetPassedParameter("popup", data) )
   #set ($popup = "false")
#end

#if(!$project)
   #set($project=$om.getProject())
#end

#macro (createTableRow $e $header $options $keys)
   #set($elm = "")  ## Clear out variable
   #set($elm = $item.getStringProperty($e))
   #dian_createTableRow($elm $header $options $keys)
#end

#macro (createSimpleTableRow $e $header)
   #set($elm = "")  ## Clear out variable
   #set($elm = $item.getStringProperty($e))
   #dian_createTableRow($elm $header "" "")
#end

<style>
#uds_form {
   border-collapse: collapse;
   border-spacing:0px;
   border: 1px solid #000;
}
#uds_form td, #uds_form th {
   vertical-align:top;
   text-align:left;
   border-top:1px solid #000;
   border-bottom:1px solid #000;
}
</style>

<table width="100%">
   <tr>
      <td>#parse($turbineUtils.getTemplateName("_report",$om.getXSIType(),$project))</td>
      <td valign="top" align="right">#elementActionsBox($element $search_field $search_value $data.getSession().getAttribute("user") $item)</td>
   </tr>
   <tr><th colspan="2">*Please note that some items are not being collected.</th></tr>
   <tr>
      <td colspan="2">
      <table id="uds_form"  width="700px">
         ## Create the Principal referral source table row
         #set ( $KEYS    = ["1","2","3","4","5","6","7","8","9","-4"])
         #set ( $OPTIONS = ["Self/relative/friend","Clinician","ADC solicitation","Non-ADC study","Clinic sample","Population sample","Non-ADC media appeal (e.g., Alzheimer's Association)","Other","Unknown","No Value Entered"])
         #createTableRow("REFER" "3. Principal referral source" $OPTIONS $KEYS)
         ## Create the Principal referral source other table row
		 #createSimpleTableRow("REFERX" "If other, specify")
	
         ## Create the Principal referral source table row
         #set ( $KEYS = ["1","2","3","-4"])
         #set ( $OPTIONS = ["Case/patient/proband","Control/normal","No presumed disease status","No Value Entered"])
         #createTableRow("PRESTAT" "4. Presumed disease status at enrollment" $OPTIONS $KEYS)
         
         ## Create the Birth Month table row
         #createSimpleTableRow("BIRTHMO" "Subject's month of birth (1-12)")
         
         ## Create the Birth Year table row
         #createSimpleTableRow("BIRTHYR" "Subject's year of birth")
         
         ## Create the subject's sex table row
         #set ( $OPTIONS = ["Male","Female","No Value Entered"])
         #set ( $KEYS = ["1","2","-4"])
         #createTableRow("SEX" "8. Subject's sex" $OPTIONS $KEYS)
         
         ## Create the Hispanic/Latino table row
         #set ( $OPTIONS = ["No","Yes", "Unknown","No Value Entered"])
         #set ( $KEYS = ["0","1","9","-4"])
         #createTableRow("HISPANIC" "9. Does the subject report being of Hispanic/Latino <u>ethnicity</u> (i.e., having origins from a mainly Spanish-speaking Latin American country), regardless of race?" $OPTIONS $KEYS)

         ## Create the subjects reported origins table row
         #set ( $OPTIONS = ["Mexican/Chicano/Mexican-American","Puerto Rican","Cuban","Dominican","Central American","South American","Other","Unknown","No Value Entered"])
         #set ( $KEYS = ["1","2","3","4","5","6","50","99","-4"])
         #createTableRow("HISPOR" "9a. If yes, what are the subject's reported origins?" $OPTIONS $KEYS)
			#createSimpleTableRow("HISPORX" "If other, specify" )
  

         ## Create the Subject's Race table row
         #set ( $OPTIONS = ["White","Black or African American","American Indian or Alaska Native","Native Hawaiian or Other Pacific Islander","Asian","Other","Unknown","No Value Entered"])
         #set ( $KEYS = ["1","2","3","4","5","50","99","-4"])
         #createTableRow("RACE" "10. What does the subject report as his/her race?" $OPTIONS $KEYS)
			#createSimpleTableRow("RACEX" "If other, specify" )
        

         ## Create the additional subject's race table row
         #set ( $OPTIONS = ["White","Black or African American","American Indian or Alaska Native","Native Hawaiian or Other Pacific Islander","Asian","Other","None Reported","Unknown","No Value Entered"])
         #set ( $KEYS = ["1","2","3","4","5","50","88","99","-4"])
         #createTableRow("RACESEC" "11. What additional race does subject report?" $OPTIONS $KEYS)
			#createSimpleTableRow("RACESECX" "If other, specify" )
     

         ## Create another additional subect's race table row
         #createTableRow("RACETER" "12. What additional race, beyond what was indicated above in questions 10 and 11, does subject report?" $OPTIONS $KEYS)
			#createSimpleTableRow("RACETERX" "If other, specify")
       

         ## Create the Subject's primary language table row
         #set ( $OPTIONS = ["English","Spanish","Mandarin","Cantonese","Russian","Japanese","Other primary language (specify)","Unknown","No Value Entered"])
         #set ( $KEYS = ["1","2","3","4","5","6","8","9","-4"])
         #createTableRow("PRIMLANG" "13. Subject's primary language" $OPTIONS $KEYS)
			#createSimpleTableRow("PRIMLANX" "Other primary language (specify)")
       

         ## Create the Subject's living situation table row
         #set ( $OPTIONS = ["Lives alone","Lives with spouse or partner","Lives with relative or friend","Lives with group","Other","Unknown","No Value Entered"])
         #set ( $KEYS = ["1","2","3","4","5","9","-4"])
         #createTableRow("LIVSIT" "15. What is the subject's living situation?" $OPTIONS $KEYS)
			#createSimpleTableRow("LIVSITX" "If other, specify")
        

         ## Create the Subject's independence level table row
         #set ( $OPTIONS = ["Able to live independently","Requires some assistance with complex activities","Requires some assistance with basic activities","Completely dependent","Unknown","No Value Entered"])
         #set ( $KEYS = ["1","2","3","4","9","-4"])
         #createTableRow("INDEPEND" "16. What is the subject's level of independence?" $OPTIONS $KEYS)
         
         ## Create the Subject's primary type of residence table row
         #set ( $OPTIONS = ["Single family residence","Retirement community","Assisted living/boarding home/adult family home","Skilled nursing facility/nursing home","Other","Unknown","No Value Entered"])
         #set ( $KEYS = ["1","2","3","4","5","9","-4"])
         #createTableRow("RESIDENC" "17. What is the subject's primary type of residence?" $OPTIONS $KEYS)
			#createSimpleTableRow("RESIDENX" "If other, specify")
       

         ## Create the Subject's primary zip code table row
         #createSimpleTableRow("ZIP" "18. Subject's primary residence zip code (first 3 digits)")
         
         ## Create the subject's current marital status table row
         #set ( $OPTIONS = ["Married","Widowed","Divorced","Separated","Never married","Living as married","Other","Unknown","No Value Entered"])
         #set ( $KEYS = ["1","2","3","4","5","6","8","9","-4"])
         #createTableRow("MARISTAT" "19. Subject's current marital status:" $OPTIONS $KEYS)
			#createSimpleTableRow("MARISTAX" "If other, specify")

         ## Create the Subject's handedness table row
         #set ( $OPTIONS = ["Left-handed","Right-handed","Ambidextrous","Unknown","No Value Entered"])
         #set ( $KEYS = ["1","2","3","9","-4"])
         #createTableRow("HANDED" "20. Is the subject left- or right-handed (for example, which hand would s/he normally use to write or throw a ball)?" $OPTIONS $KEYS)
      </table>
      </td>
   </tr>
</table>