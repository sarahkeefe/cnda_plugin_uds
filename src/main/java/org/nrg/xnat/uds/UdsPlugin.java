package org.nrg.xnat.uds;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;

@XnatPlugin(value = "cnda_plugin_uds", name = "XNAT 1.7 UDS Plugin", description = "This is the XNAT 1.7 UDS Plugin.",
        dataModels = {
                @XnatDataModel(value = "uds:a1subdemoData",
                        singular = "UDS A1: Sub Demo",
                        plural = "UDS A1: Sub Demos"),
				@XnatDataModel(value = "uds:a2infdemoData",
                        singular = "UDS A2: Informant Demos",
                        plural = "UDS A2: Informant Demos"),
				@XnatDataModel(value = "uds:a3sbfmhstData",
                        singular = "UDS A3: Partcpt Family Hist.",
                        plural = "UDS A3: Partcpt Family Hist."),
				@XnatDataModel(value = "uds:a4drugsData",
                        singular = "UDS A4: Sub Meds",
                        plural = "UDS A4: Sub Meds"),
				@XnatDataModel(value = "uds:a5subhstData",
                        singular = "UDS A5: Sub Health Hist.",
                        plural = "UDS A5: Sub Health Hist."),
				@XnatDataModel(value = "uds:b2hachData",
                        singular = "UDS B2: HIS and CVD",
                        plural = "UDS B2: HIS and CVD"),
				@XnatDataModel(value = "uds:b3updrsData",
                        singular = "UDS B3: UPDRS",
                        plural = "UDS B3: UPDRS"),
				@XnatDataModel(value = "uds:b4cdrData",
                        singular = "UDS B4: CDR",
                        plural = "UDS B4: CDRs"),
				@XnatDataModel(value = "uds:b5behavasData",
                        singular = "UDS B5: NPI-Q",
                        plural = "UDS B5: NPI-Q"),
				@XnatDataModel(value = "uds:b6bevgdsData",
                        singular = "UDS B6: GDS",
                        plural = "UDS B6: GDS"),
				@XnatDataModel(value = "uds:b6suppData",
                        singular = "UDS B6 Supplement",
                        plural = "UDS B6 Supplements"),
				@XnatDataModel(value = "uds:b7faqData",
                        singular = "UDS B7: FAQ",
                        plural = "UDS B7: FAQs"),
				@XnatDataModel(value = "uds:b8evalData",
                        singular = "UDS B8: Phys. Neuro Findings",
                        plural = "UDS B8: Phys. Neuro Findings"),
				@XnatDataModel(value = "uds:b9clinjdgData",
                        singular = "UDS B9: Clin. Judgements",
                        plural = "UDS B9: Clin. Judgements"),
				@XnatDataModel(value = "uds:b9suppData",
                        singular = "UDS B9 Supplement",
                        plural = "UDS B9 Supplements"),
				@XnatDataModel(value = "uds:d1dxData",
                        singular = "UDS D1: Clinician Diagnosis",
                        plural = "UDS D1: Clinician Diagnosis")
						})
public class UdsPlugin {
}